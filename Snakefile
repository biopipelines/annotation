from snakemake.utils import min_version

min_version("4.2.0")


rule all:
    input:
        "annotation/kggseq/selected.flt.vcf",
        "annotation/kggseq/annot.tab.gz",
        "annotation/bcftools/selected.annot.vcf",
        "annotation/bcftools/selected.annot.lightened.reheaded.vcf"


include_prefix="rules"

include:
    include_prefix + "/functions.py"
include:
    include_prefix + "/annotation.smk"
include:
    include_prefix + "/format_output.smk"
