rule gatk_SelectVariants:
    input:
        vcf=expand("{path_to_recal_vcf}/all.snp_recalibrated.indel_recalibrated.vcf", \
                     path_to_recal_vcf=config.get("paths").get("to_recalibrated_vcf"))
    output:
        vcf="variant_calling/SelectVariants/selected.vcf"
    params:
        custom=java_params(tmp_dir=config.get("paths").get("to_tmp"), fraction_for=4),
        genome=resolve_single_filepath(*references_abs_path(), config.get("genome_fasta")),
        arguments=config.get("rules").get("gatk_SelectVariants").get("arguments"),
        samples_files=config.get("rules").get("gatk_SelectVariants").get("samples_files")
    log:
        "logs/gatk/SelectVariants/SelectVariants.log"
    benchmark:
        "benchmarks/gatk/SelectVariants/SelectVariants.txt"
    threads: conservative_cpu_count()
    run:
        selection_args = _multi_flag(params.arguments)
        samples_set_arg = _get_samples_set(params.samples_files)
        shell(
            "gatk SelectVariants --java-options {params.custom} "
            "-R {params.genome} "
            "-V {input.vcf} "
            "-O {output} "
            "{selection_args} "
            "{samples_set_arg} "
            ">& {log}")


rule kggseq:
    input:
       vcf="variant_calling/SelectVariants/selected.vcf"
    output:
       vcf='annotation/kggseq/selected.flt.vcf',
       log='annotation/kggseq/selected.log',
       txt='annotation/kggseq/selected.flt.txt',
       ped='annotation/kggseq/selected.ped'
    params:
       custom=java_params(tmp_dir=config.get("paths").get("to_tmp"), fraction_for=4),
       cmd=config.get("rules").get("kggseq").get("cmd"),
       arguments=config.get("rules").get("kggseq").get("arguments"),
       ped_file=config.get("rules").get("kggseq").get("ped_file")
    benchmark:
        "benchmarks/kggseq/kggseq.txt"
    threads: conservative_cpu_count()
    run:
        kggseq_args = _multi_flag(params.arguments)
        shell(
            "cp {params.ped_file} {output.ped} && "
            "{params.cmd} -nt {threads} {params.custom} "
            "--no-resource-check --no-lib-check --no-web --no-gz "
            "--no-qc --o-vcf "
            "{kggseq_args} "
            "--vcf-file {input.vcf} "
            "--ped-file {output.ped} "
            "--out annotation/kggseq/selected")
